<h1 style="text-align: center">浙江大学自动健康打卡AutoCard</h1>
<div style="text-align: center">

![AUR](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg)
![star](https://gitee.com/GCSZHN/AutoCard-Android/badge/star.svg?theme=white)
![GitHub stars](https://img.shields.io/github/stars/GCS-ZHN/AutoCard-Android.svg?style=social&label=Stars)
![GitHub forks](https://img.shields.io/github/forks/GCS-ZHN/AutoCard-Android.svg?style=social&label=Fork)

</div>

## 项目概述
本项目为解决浙江大学每日重复的健康打卡而开发，在完成首次手动打卡后，可以自动进行定时打卡。是[AutoCard项目](https://github.com/GCS-ZHN/AutoCard)在安卓APP上的拓展。旨在满足没有服务器资源的时候能够实现定时打卡。

![AutoCard](fig/fig1.png)

## 基本使用步骤
1. 一部持续运行的手机

要求Android 6.0（SDK 23）及以上，支持后台运行白名单。不是省电模式，联网（最好是移动数据，WIFI容易断，没网会中断服务）。

2. 下载安装AutoCard

从[github](https://github.com/GCS-ZHN/AutoCard-Android/releases)或[gitee](https://gitee.com/GCSZHN/AutoCard-Android/releases)的项目发行版下载apk安装包，在手机里安装。安装完成后，将应用放在手机的后台运行白名单里。

以华为P9（16年发布的机型）为例，在“设置->电池->启动管理”里面，选择手动管理，允许后台运行。否则，锁屏之后应用会被挂起而无法定时。

同时给予通知权限，会在任务栏发送打卡结果通知。

- 打卡成功
- 打卡失败
- 重复打卡

3. 启动AutoCard

首次启动会弹出提示请求忽略电池优化，建议选是，原因和前面白名单一致。初次启动需要手动填写你的账户信息。然后选择定时时间，按电源符号的按钮启动服务（大约有几秒延时，没做登录时动画效果）。若登录失败会提示，失败原因可能是密码错误，也可以只是登录暂时性被浙大后台拒绝（毕竟非官方而是爬虫手段）。成功后跳转打卡页面

4. 开始打卡

进入打卡页面后，你就啥也不用干了，你会看到你自己旋转头像，会在通知栏看到一个删不掉的通知（Google要求前台服务必须显示，就像音乐播放器那样），说明打卡服务在运行。通过手机“HOME”键回到桌面，让它后台运行就好（不要手欠将程序卡片上划）。此时的那个电源按钮是用来中断服务的，不要按它。

## 其他问题
1. 如何判断应用是否运行中而不是被挂起？

前台服务虽然不容易被挂起，但是难保手机休眠时会被挂起。挂起或运行的应用，用户都能看到那个程序卡片（上划彻底关闭程序的那个）。为了判断是否运行，可以选择一个较近的时间定时（也不要太近，比如下一秒，这样时间实际上当运行时，今天已经过了点）。然后返回桌面，等时间到了，查看APP或者任务栏通知。

2. 不支持后台运行的手机咋办？

就得你自己点进去APP了，和钉钉打卡区别可能就是不用自己填答案。

3. 手机耗电？

显然长时间定时会明显耗电，只能说及时充电吧。咱这个APP不像系统自己的闹钟，不保持运行是无法定时的。系统闹钟由系统提供，系统运行闹钟自然就有。有条件还是去租赁一个服务器，这样完全不用操心没电问题。

4. 信息安全？
   
虽然本APP用浙大通行证，但是直接与浙大自己的服务器交流（爬虫），并没有通过作者自己的服务器，因此不用担心这个问题。源码也在项目中，有兴趣的同学可以阅读。

5. 手机安装失败？闪退？

因为是个人业余开发，只在自己的华为平板（Android 6.0, SDK 23）、华为手机旧机（Android 8.0, SDK 26）和安卓模拟器（Android 10+, SDK 30）上测试，因此无法确定其他安卓机子的适配情况。安卓不比iOS，安卓的个性化版本太多。有问题欢迎发issue交流。

