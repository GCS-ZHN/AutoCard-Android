package org.gcszhn.autocard;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.gcszhn.autocard.service.FileService;
import org.gcszhn.autocard.utils.LogUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class FileServiceTest {
    FileService fileService;
    @Before
    public void before() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        fileService = new FileService(appContext);
    }
    @Test
    public void writeTest() {
        fileService.write("test.txt", "hello world".getBytes());
    }
    @Test
    public  void readTest() {
        LogUtils.printMessage(new String(fileService.read("test.txt")));
    }
}
