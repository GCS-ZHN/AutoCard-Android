/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

/**
 * 服务管理工具
 * @author Zhang.H.N
 * @version 1.0
 * */
public class ServiceUtils {
    /**
     * 开启服务
     * @param context
     * @param serviceClass
     * @param action
     * */
    public static void startService(Context context, Class<? extends Service> serviceClass, String action){
        startService(context, serviceClass, action, null);
    }
    public static void startService(Context context, Class<? extends Service> serviceClass, String action, Bundle data) {
        Intent intent = new Intent(context, serviceClass);
        if (action != null) intent.setAction(action);
        if (data!=null) intent.putExtras(data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }
    /**
     * 关闭服务
     * @param context
     * @param serviceClass
     * */
    public static void stopService(Context context, Class<? extends Service> serviceClass) {
        context.stopService(new Intent(context, serviceClass));
    }
}
