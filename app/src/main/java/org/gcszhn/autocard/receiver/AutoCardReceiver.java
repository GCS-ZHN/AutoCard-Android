/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.gcszhn.autocard.activity.RunActivity;
import org.gcszhn.autocard.config.AppConfig;
import org.gcszhn.autocard.config.UserConfig;
import org.gcszhn.autocard.service.AutoCardService;
import org.gcszhn.autocard.service.ClockinService;
import org.gcszhn.autocard.utils.LogUtils;
import org.gcszhn.autocard.utils.ServiceUtils;
import org.gcszhn.autocard.utils.StatusCode;

/**
 * 自动打卡服务广播接收器
 * */
public class AutoCardReceiver extends BroadcastReceiver implements Runnable {
    /**每次传入的context*/
    private Context context;
    /** 用户名*/
    private String username =  UserConfig.getUser();
    /** 密码*/
    private String password = UserConfig.getPassword();
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        try {
            // 执行任务
            AppConfig.THREAD_POOL.execute(this);
        } catch (Exception e) {
            LogUtils.printMessage(null, e, LogUtils.Level.ERROR);
        }
    }
    /**打卡任务的Runnable实现*/
    @Override
    public void run() {
        RunActivity.sendMessage("本轮打卡中...");
        int change = 3;
        LogUtils.printMessage("Automatic clock-in job starts...");
        try (ClockinService cardService = new ClockinService()) {
            if (username==null||password==null)
                throw new NullPointerException("Empty username or password of zjupassport");
            // 打卡 三次机会
            StatusCode statusCode = null;
            while (change>0 && (statusCode = cardService.submit(username, password)).getStatus()==-1) {
                int delay = (4-change) * 10;
                LogUtils.printMessage("Try to submit again after sleeping "+delay+"s ...",
                        LogUtils.Level.ERROR);
                Thread.sleep(delay * 1000);
                change--;
            }
            if (change==0) {
                LogUtils.printMessage("Submit failed 3 times for " + username,
                        LogUtils.Level.ERROR);
            }
            AutoCardService.getHasJob().set(false);

            // 开启下一次任务, 并发送通知
            Bundle data = new Bundle();
            data.putCharSequence("message", statusCode.getMessage());
            ServiceUtils.startService(
                context,
                AutoCardService.class,
                AutoCardService.ACTION_REPEAT_CARD,
                data
            );
        } catch (Exception e) {
            LogUtils.printMessage(e.getMessage(), LogUtils.Level.ERROR);
        }
    }
}
