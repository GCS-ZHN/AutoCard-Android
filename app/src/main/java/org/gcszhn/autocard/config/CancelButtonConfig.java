/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.config;

import android.app.Activity;
import android.view.View;
import android.widget.Button;

import org.gcszhn.autocard.service.AutoCardService;
import org.gcszhn.autocard.utils.LogUtils;
import org.gcszhn.autocard.utils.ServiceUtils;

/**
 * 取消服务按钮的配置类
 * @author Zhang.H.N
 * @version 1.0
 * */
public class CancelButtonConfig {
    /**按钮实例组件*/
    private static Button button = null;
    private static Activity activity = null;
    /**
     * 配置方法
     * @param button 按钮实例
     * @param activity 调用该方法的Activity实例
     * */
    public static void config(final Button button, final Activity activity) {
        CancelButtonConfig.button = button;
        CancelButtonConfig.activity = activity;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }
    /**
     * 取消按钮
     * */
    public static void cancel() {
        try {
            if (activity==null) return;
            ServiceUtils.stopService(activity, AutoCardService.class);
            activity.finish();  //结束当前activity，回到登录activity
        } catch (Exception e) {
            LogUtils.printMessage(null, e, LogUtils.Level.ERROR);
        }
    }
}
