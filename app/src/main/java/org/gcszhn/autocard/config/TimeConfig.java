/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.config;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;

import org.gcszhn.autocard.R;
import org.gcszhn.autocard.utils.LogUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间配置管理类
 * @author Zhang.H.N
 * @version 1.0
 * */
public class TimeConfig {
    private static boolean initialized = false;
    /**时间字符化格式*/
    private  static SimpleDateFormat timeFormat;
    /**日期+时间格式化*/
    private static SimpleDateFormat dateTimeFormat;
    /**定时时间，即页面显示的时间*/
    private static Calendar firstSchedule;
    /**定时时间，会随着周期任务，逐步增长，用于创建Android闹钟服务*/
    private static Calendar autoIncreaseSchedule;
    /**时间选择器*/
    private static TimePickerView timePickerView;
    /**定时周期单位*/
    private static int timePeriodUnit;
    /**定时周期*/
    private static int timePeriod;
    /**
     * 时间配置方法
     * @param editText 时间文本编辑器
     * @param activity Activity实例
     * */
    public static void config(final EditText editText, Activity activity) {
        //初始化定时配置
        if (!initialized) {
            autoIncreaseSchedule = Calendar.getInstance();
            firstSchedule = Calendar.getInstance();
            timeFormat = new SimpleDateFormat(activity.getString(R.string.time_format));
            dateTimeFormat = new SimpleDateFormat(activity.getString(R.string.date_time_format));
            String timeUnitString = activity.getString(R.string.time_period_unit);
            timePeriod = activity.getResources().getInteger(R.integer.time_period);
            timePeriodUnit = Calendar.DAY_OF_YEAR;
            switch (timeUnitString) {
                case "second": timePeriodUnit = Calendar.SECOND;break;
                case "minute": timePeriodUnit = Calendar.MINUTE;break;
                case "hour"  : timePeriodUnit = Calendar.HOUR_OF_DAY;break;
                case "day"   : timePeriodUnit = Calendar.DAY_OF_YEAR;break;
                default:{
                    LogUtils.printMessage("time unit " + timeUnitString + " is not supported");
                }
            }
            timeSync();
        }
        initialized = true;
        timePickerView = new TimePickerBuilder(activity, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                editText.setText(timeFormat.format(date));
                firstSchedule.setTime(date);
            }
            })  .setTitleText(activity.getString(R.string.time_select_title))
                .setSubmitText(activity.getString(R.string.time_select_submit))
                .setCancelText(activity.getString(R.string.time_select_cancel))
                .isCyclic(true)
                .setType(new boolean[]{false, false, false, true, true, true})
                .setTitleBgColor(activity.getColor(R.color.time_select_title_bg))
                .setTextColorCenter(activity.getColor(R.color.infoFontColor))
                .setSubmitColor(activity.getColor(R.color.infoFontColor))
                .setCancelColor(activity.getColor(R.color.infoFontColor))
                .setTitleColor(activity.getColor(R.color.infoFontColor))
                .setBgColor(activity.getColor(R.color.time_select_body_bg))
        .build();
        editText.setText(timeFormat.format(new Date(firstSchedule.getTimeInMillis())));
        timePickerView.setDate(firstSchedule);
        editText.setFocusable(false);
        editText.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                timePickerView.show();
                return false;
            }
        });
    }
    /**
     * 将增长时间同步回初始时间，确保按下开始按钮时，两者一致
     * */
    public static void timeSync() {
        Calendar current = Calendar.getInstance();
        firstSchedule.set(Calendar.YEAR, current.get(Calendar.YEAR));
        firstSchedule.set(Calendar.DAY_OF_YEAR, current.get(Calendar.DAY_OF_YEAR));
        if (timePickerView != null)timePickerView.setDate(firstSchedule);
        autoIncreaseSchedule.setTimeInMillis(firstSchedule.getTimeInMillis());
    }
    /**
     * 根据时间周期，更新下一次的时间，由AutoCardService调用
     * */
    public synchronized static long nextTimeUpdate() {
        while (autoIncreaseSchedule.compareTo(Calendar.getInstance()) < 0) {
            autoIncreaseSchedule.add(timePeriodUnit, timePeriod);
        }
        return autoIncreaseSchedule.getTimeInMillis();
    }
    public static String nextTimeFormat() {
        return dateTimeFormat.format(new Date(autoIncreaseSchedule.getTimeInMillis()));
    }
    public static String getCurrentTimeFormat() {
        Date date = new Date();
        return dateTimeFormat.format(date);
    }
}
