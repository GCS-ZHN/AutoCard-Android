/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.config;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import org.gcszhn.autocard.R;
import org.gcszhn.autocard.utils.LogUtils;

import lombok.Getter;

/**
 * 用户信息输入框配置实例
 * @author Zhang.H.N
 * @version 1.0
 */
public class UserConfig {
    /**用户属性值是否初始化*/
    private static boolean initialized = false;
    /**用户名输入框组件*/
    private @Getter static EditText userText;
    /**密码输入框组件*/
    private @Getter static EditText passwordText;
    /**用户名*/
    private @Getter static String user = "";
    /**密码*/
    private @Getter static String password = "";
    /**用户名是否准确好，即非空*/
    private static boolean userReady = false;
    /**密码是否准备好*/
    private static boolean passwordReady = false;
    /**APP内容，一般是Activity*/
    private static Activity activity = null;
    /**头像二进制数据*/
    private static byte[] headerBytes = null;
    /**用户头像*/
    private @Getter static Bitmap header = null;
    /**
     * 初始化用户名输入组件
     * @param userText 用户名输入组件
     * @param passwordText 密码输入组件
     * @param activity Activity实例
     * */
    public static void config(final EditText userText, final EditText passwordText, Activity activity) {
        UserConfig.userText = userText;
        UserConfig.passwordText = passwordText;
        UserConfig.activity = activity;
        if (!initialized) {
            loadCache();
            user = userText.getText().toString();
            password = passwordText.getText().toString();
            LogUtils.printMessage("First initialize user configuration", LogUtils.Level.DEBUG);
        } else {
            userText.setText(user);
            passwordText.setText(password);
            initialized = true;
        }
        userReady = !user.isEmpty();
        passwordReady = !password.isEmpty();
        userText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                user = s.toString();
                if (user.isEmpty()){
                    userReady = false;
                } else {
                    userReady = true;
                }
            }
        });
        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                password = s.toString();
                if (password.isEmpty()){
                    passwordReady = false;
                } else {
                    passwordReady = true;
                }
            }
        });
        userText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                userText.setCursorVisible(true);
                return false;
            }
        });
        passwordText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                passwordText.setCursorVisible(true);
                return false;
            }
        });
    }
    public static boolean isReady() {
        return userReady && passwordReady;
    }
    /**
     * 加载用户信息缓存
     * */
    private static void loadCache() {
        SharedPreferences pref = activity.getSharedPreferences(
                activity.getString(R.string.user_pref), Activity.MODE_PRIVATE
        );
        userText.setText(pref.getString("user", ""));
        passwordText.setText(pref.getString("password", ""));
        String headerString = pref.getString("header", null);
        if (headerString != null) {
            byte[] image = Base64.decode(headerString, Base64.DEFAULT);
            setHeader(image);
        }
    }
    /**
     * 写入用户信息缓存
     * */
    public static void writeCache() {
        SharedPreferences pref = activity.getSharedPreferences(
                activity.getString(R.string.user_pref), Activity.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("user", user);
        editor.putString("password", password);
       if (headerBytes!=null)editor.putString("header", Base64.encodeToString(headerBytes, Base64.DEFAULT));
        editor.commit();
    }
    public static void setHeader(byte[] headerBytes) {
        if (headerBytes != null) {
            UserConfig.headerBytes = headerBytes;
            header = BitmapFactory.decodeByteArray(headerBytes, 0, headerBytes.length);
        }

    }
}
