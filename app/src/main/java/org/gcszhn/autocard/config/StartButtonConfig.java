/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.config;

import android.content.Intent;
import android.util.Base64;
import android.view.View;
import android.widget.Button;

import com.alibaba.fastjson.JSONObject;

import org.gcszhn.autocard.activity.LoginActivity;
import org.gcszhn.autocard.activity.RunActivity;
import org.gcszhn.autocard.component.AutoCardApp;
import org.gcszhn.autocard.service.AutoCardService;
import org.gcszhn.autocard.service.FileService;
import org.gcszhn.autocard.service.ToastService;
import org.gcszhn.autocard.service.ZJUClientService;
import org.gcszhn.autocard.utils.LogUtils;
import org.gcszhn.autocard.utils.ServiceUtils;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * 开始按钮的配置类
 * @author Zhang.H.N
 * @version 1.0
 */
public class StartButtonConfig {
    /**线程锁*/
    private static Lock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();
    private static String getPhoteUrl = "http://my.zju.edu.cn/_web/fusionportal/zjuGetPhoto.jsp?_p=";
    private static Pattern keyPattern = Pattern.compile("http://my.zju.edu.cn/_web/fusionportal/index.jsp\\?_p=(.+?)\"");
    private static boolean loginStatus;
    /**
     * 配置方法
     * @param button 按钮实例
     * @param loginActivity 调用该方法的LoginActivity实例
     * */
    public static void config(final Button button, final LoginActivity loginActivity) {
        FileService fileService = new FileService(loginActivity);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(!AutoCardApp.checkNetwork(true)) return;
                    AutoCardApp.powerSaveModeWarning(false);
                    button.setEnabled(false);
                    TimeConfig.timeSync();
                    if (!UserConfig.isReady()) {
                        ToastService.sendToast("用户名或密码不能为空");
                        return;
                    }
                    tryLogin();
                    if (loginStatus) {
                        UserConfig.writeCache(); // 缓存用户数据
                        loginActivity.refreshHead();  // 更新登录页面头像
                        loginActivity.startActivity(new Intent(loginActivity, RunActivity.class));  //跳转
                        ServiceUtils.startService(loginActivity, AutoCardService.class, AutoCardService.ACTION_INIT_CARD); //开启服务
                    } else {
                        ToastService.sendToast("登录失败，请检查");
                    }
                } catch (Exception e) {
                    LogUtils.printMessage(null, e, LogUtils.Level.ERROR);
                } finally {
                    button.setEnabled(true);
                }
            }
        });
        button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                loginActivity.getApplication().onTerminate();
                return true; //忽视事件链后面的单击事件
            }
        });
    }
    private static void tryLogin() {
        try{
            lock.lock();
            AppConfig.THREAD_POOL.execute(new Runnable() {
                @Override
                public void run() {
                    ZJUClientService zjuClientService = new ZJUClientService();
                    boolean status = zjuClientService.login(UserConfig.getUser(), UserConfig.getPassword());
                    //更新头像
                    if (status) {
                        String html = zjuClientService.doGetText("http://my.zju.edu.cn/");
                        Matcher matcher = keyPattern.matcher(html);
                        if (matcher.find()) {
                            String key = matcher.group(1);
                            String target = getPhoteUrl +key;
                            ArrayList<NameValuePair> param = new ArrayList<>(1);
                            param.add(new BasicNameValuePair("loginName", UserConfig.getUser()));
                            html = zjuClientService.doPostText(target, param);
                            JSONObject jsonObject = JSONObject.parseObject(html);
                            String base64Pic = jsonObject.getString("reason")
                                    .replaceFirst("data:image/gif;base64,","");
                            byte[] image = Base64.decode(base64Pic, Base64.DEFAULT);
                            UserConfig.setHeader(image);
                        }
                    }
                    loginStatus = status;
                    lock.lock();
                    condition.signal();
                    lock.unlock();
                }
            });
            condition.await();
        } catch (Exception e) {
            LogUtils.printMessage(null, e, LogUtils.Level.ERROR);
        }
        lock.unlock();
    }
}
