/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.component;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;

import org.gcszhn.autocard.config.CancelButtonConfig;
import org.gcszhn.autocard.receiver.NetWorkReceiver;
import org.gcszhn.autocard.service.AutoCardService;
import org.gcszhn.autocard.service.NotificationService;
import org.gcszhn.autocard.service.ToastService;
import org.gcszhn.autocard.utils.LogUtils;

import java.util.Stack;

import lombok.Getter;

/**
 * AutoCard应用程序类
 * @author Zhang.H.N
 * @version 1.0
 * */
public class AutoCardApp extends Application {
    /**Application Context*/
    private @Getter static Context context;
    /**动态注册的广播栈*/
    private Stack<BroadcastReceiver> broadcastReceivers = new Stack<>();

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        // 动态注册网络变化广播
        IntentFilter networkChangeFilter = new IntentFilter();
        networkChangeFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        NetWorkReceiver netWorkChangeReceiver = new NetWorkReceiver();//网络变化广播
        registerReceiver(netWorkChangeReceiver, networkChangeFilter);
    }

    @Override
    public Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        broadcastReceivers.addElement(receiver);
        return super.registerReceiver(receiver, filter);
    }

    /** 注销动态注册广播 */
    @Override
    public void onTerminate() {
        super.onTerminate();
        while (!broadcastReceivers.empty()) {
            unregisterReceiver(broadcastReceivers.pop());
        }
    }
    /**
     * 检查网络状态
     * @param goToSeting 是否跑去设置页面
     * */
    public static boolean checkNetwork(boolean goToSeting) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network activeNetwork = connectivityManager.getActiveNetwork();
        if (activeNetwork == null) {
            ToastService.sendToast("当前网络不可用");
            if (goToSeting) {
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                context.startActivity(intent);
            }
            if (AutoCardService.isRunning()) {
                NotificationService.sendNotification("服务通知", "网络不可用，服务断开");
            }
            CancelButtonConfig.cancel();  //没网则中断服务
        }
        return activeNetwork != null;
    }
    /**
     * 是否节电模式
     * */
    public static boolean isPowerSaveMode() {
        if (Build.MANUFACTURER.equalsIgnoreCase("huawei")) {
            try {
                return 4 == Settings.System.getInt(context.getContentResolver(), "SmartModeStatus");
            } catch (Exception e) {
                LogUtils.printMessage(null ,e, LogUtils.Level.ERROR);
            }
        }
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        return powerManager.isPowerSaveMode();
    }
    /**
     * 节电模式警告
     * */
    public static void powerSaveModeWarning(boolean notify) {
        String msg = "后台定时运行服务，建议关闭省电模式";
        if (isPowerSaveMode()) {
            if (notify) {
                NotificationService.sendNotification("服务提醒", msg);
            }
            ToastService.sendToast(msg);
        }
    }
}
