/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.component;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

/**
 * 创建图像动画
 * @author Zhang.H.N
 * @version 1.0
 */
public class AnimImage {
    /**图像实例组件*/
    private ImageView image;
    /**动画*/
    private Animation animation;
    /**
     * @param imageView 图像实例组件
     * @param context 内容组件
     * @param id 动画实例id
     * */
    public AnimImage(ImageView imageView, Context context, int id) {
        animation = AnimationUtils.loadAnimation(context, id);
        animation.setInterpolator(new LinearInterpolator());
        image = imageView;
    }
    /**暂停动画*/
    public void stop() {
        animation.reset();
        image.clearAnimation();
    }
    /**开始动画*/
    public void start() {
        stop();
        image.startAnimation(animation);
    }
}
