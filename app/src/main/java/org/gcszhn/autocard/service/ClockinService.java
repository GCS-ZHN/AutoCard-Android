/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.
 *
 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at
 *
 *       http://wwww.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */
package org.gcszhn.autocard.service;

import com.alibaba.fastjson.JSONObject;

import org.gcszhn.autocard.utils.LogUtils;
import org.gcszhn.autocard.utils.StatusCode;

import java.io.Closeable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * 健康打卡实现类
 * @author Zhang.H.N
 * @version 1.2
 */
public class ClockinService implements Closeable {
    /**打卡信息网址 */
    private String reportUrl = "https://healthreport.zju.edu.cn/ncov/wap/default/index";
    /**打卡提交网址 */
    private String submitUrl = "https://healthreport.zju.edu.cn/ncov/wap/default/save";
    /**浙大通行证客户端 */
    private ZJUClientService client = new ZJUClientService();
    /**时间格式化 */
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    /**
     * 用于访问打卡页面
     * @param username 用户名
     * @param password 密码
     * @return 打卡页面HTML源码
     */
    public String getPage(String username, String password) {
        if(client.login(username, password)) {
            return client.doGetText(reportUrl);
        }
        return null;
    }
    /**
     * 用于提取已有提交信息
     * @param username 用户名
     * @param password 密码
     * @return 已有提交信息组成的键值对列表
     */
    public ArrayList<NameValuePair> getOldInfo(String username, String password) {
        String page = getPage(username, password);
        if (page==null) return null;
        final ArrayList<NameValuePair> res = new ArrayList<>();
        try {
            Pattern defPattern = Pattern.compile("var def = (\\{.+\\});");
            Matcher matcher = defPattern.matcher(page);
            JSONObject defJsonObject = null;
            if (matcher.find()) {
                defJsonObject = JSONObject.parseObject(matcher.group(1));
            } else {
                return null;
            }
            Pattern info = Pattern.compile("\\$\\.extend\\((\\{.+?\\}), def, (\\{.+?\\})\\)", Pattern.DOTALL);
            JSONObject infoJsonObject1 = null;
            JSONObject infoJsonObject2 = null;
            matcher = info.matcher(page);
            if (matcher.find()) {
                infoJsonObject1 = JSONObject.parseObject(matcher.group(1));
                infoJsonObject2 = JSONObject.parseObject(matcher.group(2));
            } else {
                return null;
            }
            Pattern oldInfoPattern = Pattern.compile("oldInfo: (\\{.+\\})");
            matcher = oldInfoPattern.matcher(page);
            JSONObject oldInfoJson = null;
            if (matcher.find()) {
                oldInfoJson = JSONObject.parseObject(matcher.group(1));
            } else {
                return null;
            }
            infoJsonObject1.putAll(defJsonObject);
            infoJsonObject1.putAll(infoJsonObject2);
            infoJsonObject1.putAll(oldInfoJson);
            for (Map.Entry<String, Object> entry: infoJsonObject1.entrySet()) {
                String name = entry.getKey();
                Object value = entry.getValue();
                switch (name) {
                    case "date":value=sdf.format(new Date());break;
                }
                if (value.toString().equals("[]")) continue; //空数组不上报
                res.add(new BasicNameValuePair(name, String.valueOf(value)));
            }
        } catch (Exception e) {
            LogUtils.printMessage(null, e, LogUtils.Level.ERROR);
        }
        return res;
    }
    /**
     * 用于提交打卡信息
     * @param username 用户名
     * @param password 密码
     * @return 打卡状态
     *  0   打卡成功
     *  1   今日已经打卡
     * -1   打卡失败
     */
    public StatusCode submit(String username, String password) {
        StatusCode statusCode = new StatusCode();
        LogUtils.printMessage("Try to submit for " + username);
        ArrayList<NameValuePair> info = getOldInfo(username, password);
        if (info==null) {
            LogUtils.printMessage("Submit failed", LogUtils.Level.ERROR);
            statusCode.setMessage(username+", 打卡信息获取失败");
            statusCode.setStatus(-1);
            return statusCode;
        }
        JSONObject resp = JSONObject.parseObject(client.doPostText(submitUrl, info));
        int status = resp.getIntValue("e");
        LogUtils.Level level = null;
        switch(status) {
            case 0:{level= LogUtils.Level.INFO;break;}
            case 1:{level= LogUtils.Level.ERROR;break;}
        }
        statusCode.setStatus(status);
        statusCode.setMessage(username+","+resp.getString("m"));
        LogUtils.printMessage(resp.getString("m"), level);
        LogUtils.printMessage("Finish info submit...", LogUtils.Level.DEBUG);
        return statusCode;
    }
    @Override
    public void close() {
        try {
            client.close();
        } catch (Exception e) {
            LogUtils.printMessage(null, e, LogUtils.Level.ERROR);
        }
    }
}
