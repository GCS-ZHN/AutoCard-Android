/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.service;

import android.os.Build;
import android.view.Gravity;
import android.widget.Toast;

import org.gcszhn.autocard.component.AutoCardApp;

/**
 * Toast 服务
 * @author Zhang.H.N
 * @version  1.0
 * */
public class ToastService {
    /**
     * 发送toast
     * @param msg 信息
     * */
    public static void sendToast(String msg) {
        Toast toast = Toast.makeText(AutoCardApp.getContext(), msg, Toast.LENGTH_SHORT);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {  // SDK 30 开始不允许Text Toast设置
            toast.setGravity(Gravity.TOP, 0, 0);
        }
        toast.show();
    }
}
