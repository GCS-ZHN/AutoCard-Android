/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.service;

import android.content.Context;

import org.gcszhn.autocard.utils.LogUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileService {
    private Context context;
    public FileService(Context context) {
        this.context = context;
    }
    public boolean write(String filename, byte[] content) {
        try (FileOutputStream fileOutputStream = context.openFileOutput(filename, Context.MODE_PRIVATE)) {
            fileOutputStream.write(content);
            return true;
        } catch (Exception e) {
            LogUtils.printMessage(null, e, LogUtils.Level.ERROR);
            return false;
        }
    }
    public byte[] read(String filename) {
        try(FileInputStream inputStream = context.openFileInput(filename);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int len;
            while ((len=inputStream.read(buffer))!=-1) {
                byteArrayOutputStream.write(buffer,0, len);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            LogUtils.printMessage(null, e, LogUtils.Level.ERROR);
        }
        return null;
    }
}
