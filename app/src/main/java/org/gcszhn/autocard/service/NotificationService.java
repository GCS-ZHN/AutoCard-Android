/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */


package org.gcszhn.autocard.service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import org.gcszhn.autocard.R;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 通知服务
 * @author Zhang.H.N
 * @version 1.0
 * */
public class NotificationService {
    /**是否振动*/
    private static boolean vibrate = true;
    /**
     * 通知管理器
     * */
    private static NotificationManager manager;
    /**
     * ID实例
     * */
    private static AtomicInteger notificationId = new AtomicInteger(1);
    /**
     * 活动实例
     * */
    private static Activity context;
    /**
     * 初始化通知服务
     * @param activity 活动实例
     * */
    public static void init(Activity activity) {
        manager = (NotificationManager) activity.getSystemService(Activity.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notification", "打卡通知", NotificationManager.IMPORTANCE_HIGH);
            channel.setBypassDnd(true);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.enableVibration(vibrate);
            manager.createNotificationChannel(channel);
        }
        context = activity;
    }
    /**
     * 发送通知
     * @param title 标题
     * @param content 内容
     * */
    public static void sendNotification(String title, String content) {
        manager.notify(getNotificationId(), getNotification(title, content));
    }
    /**
     * 获取通知ID
     * */
    public static int getNotificationId() {
        return notificationId.incrementAndGet();
    }
    /**
     * 获取通知实例
     * @param title 标题
     * @param content 内容
     * */
    public static Notification getNotification(String title, String content) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "notification")
            .setContentTitle(title)
            .setContentText(content)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setSmallIcon(R.drawable.ic_username);
        if (vibrate) {
            builder.setVibrate(new long[]{0, 1000, 1000, 1000});
        }
        return builder.build();
    }
}
