/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;

import org.gcszhn.autocard.activity.RunActivity;
import org.gcszhn.autocard.config.TimeConfig;
import org.gcszhn.autocard.receiver.AutoCardReceiver;
import org.gcszhn.autocard.utils.LogUtils;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Getter;

/**
 * 自动打卡服务
 * @author Zhang.H.N
 * @version 1.0
 * */
public class AutoCardService extends Service {
    /**执行首次打卡*/
    public static final String ACTION_INIT_CARD = "org.gcszhn.autocard.intent.initCard";
    /**执行打卡任务的ACTION*/
    public static final String ACTION_REPEAT_CARD = "org.gcszhn.autocard.intent.repeatCard";
    /**唤醒服务的ACTION*/
    public static final String ACTION_WAKE_UP = "org.gcszhn.autocard.intent.wakeup";
    /**是否有任务待完成*/
    private @Getter final static AtomicBoolean hasJob = new AtomicBoolean(false);
    /**服务是否运行*/
    private @Getter static boolean running = false;
    /**闹钟服务管理器*/
    private AlarmManager alarmManager;
    /**电源服务管理器*/
    private PowerManager powerManager;
    /**电源锁*/
    private PowerManager.WakeLock wakeLock;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        //初始化管理器
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        //获取电源锁
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AutoCard:AutoCardService");
        wakeLock.acquire();

        running = true;
        hasJob.set(false);  // 服务刚开始，job必须为0
        LogUtils.printMessage("Service created");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent nextIntent = new Intent(this, AutoCardReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this, 0, nextIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.cancel(pendingIntent);
        stopForeground(true);
        wakeLock.release();
        hasJob.set(false);
        running = false;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //启动前台服务的配套, 同时利用它发送通知
        String message;
        try {
            message = intent.getExtras().getCharSequence("message").toString();;
        } catch (NullPointerException e) {
            message = "AutoCard服务已开启";
        }
        startForeground(1, NotificationService.getNotification("服务通知", message));
        if (intent != null) {
            switch(intent.getAction()) {
                case ACTION_INIT_CARD: return autoCard(null);
                case ACTION_REPEAT_CARD: return autoCard(message);
                case ACTION_WAKE_UP: return START_STICKY;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }
    /**
     * 自动打卡
     * */
    private int autoCard(String lastResult) {
        if (getHasJob().get()) return START_STICKY;
        // 定义意图为广播通知
        Intent nextIntent = new Intent(this, AutoCardReceiver.class);
        nextIntent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        PendingIntent pi = PendingIntent.getBroadcast(
                this, 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // 周期定时

        getHasJob().set(true);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, TimeConfig.nextTimeUpdate(), pi);
        String message;
        if (lastResult == null|| lastResult.isEmpty()) {
            message = "下轮开始：" + TimeConfig.nextTimeFormat();
        } else {
            message = lastResult + "\n" +
                    "本轮完成：" + TimeConfig.getCurrentTimeFormat() + "\n"+
                    "下轮开始：" + TimeConfig.nextTimeFormat();
        }
        RunActivity.sendMessage(message);
        return START_STICKY;
    }
}
