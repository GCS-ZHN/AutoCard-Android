/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.gcszhn.autocard.R;
import org.gcszhn.autocard.component.AutoCardApp;
import org.gcszhn.autocard.config.StartButtonConfig;
import org.gcszhn.autocard.config.TimeConfig;
import org.gcszhn.autocard.config.UserConfig;
import org.gcszhn.autocard.service.AutoCardService;
import org.gcszhn.autocard.service.NotificationService;

/**
 * 程序的入口Activity，即登录页面
 * @author Zhang.H.N
 * @version 1.2
 * */
public class LoginActivity extends BasicActivity {
    /**用户名编辑器组件*/
    private EditText userText;
    /**密码编辑器组件*/
    private EditText passwordText;
    /**时间编辑器组件*/
    private EditText timerText;
    /**主按钮组件*/
    private Button startButton;
    /**头像组件*/
    private ImageView header;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //初始化
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        NotificationService.init(this);

        //组件配置
        userText = findViewById(R.id.userText);
        passwordText = findViewById(R.id.passwdText);
        timerText = findViewById(R.id.timerText);
        startButton = findViewById(R.id.startButton);
        header = findViewById(R.id.header);
        header.setBackground(getDrawable(R.drawable.ic_head));
        UserConfig.config(userText, passwordText, this);
        TimeConfig.config(timerText, this);
        StartButtonConfig.config(startButton, this);
        refreshHead();

        //提示刷新
        AutoCardApp.checkNetwork(false);
        AutoCardApp.powerSaveModeWarning(false);
        ignoreBatteryOptim();

        //条件跳转
        if (AutoCardService.isRunning()) startActivity(new Intent(this, RunActivity.class));
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        UserConfig.writeCache();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        hiddenInput();
        return super.onTouchEvent(event);
    }
    /**更新头像*/
    public void refreshHead() {
        if (UserConfig.getHeader()!=null) header.setImageBitmap(UserConfig.getHeader());
    }
    /**触空屏隐藏输入法*/
    public void hiddenInput() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(timerText.getWindowToken(), 0);
    }
    /**忽略电池优化*/
    public void ignoreBatteryOptim() {
        Intent intent = new Intent();
        String packageName = getPackageName();
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (!powerManager.isIgnoringBatteryOptimizations(packageName)) {
            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:"+packageName));
            startActivity(intent);
        }
    }
}