/*
 * Copyright © 2021 <a href="mailto:zhang.h.n@foxmail.com">Zhang.H.N</a>.

 * Licensed under the Apache License, Version 2.0 (thie "License");
 * You may not use this file except in compliance with the license.
 * You may obtain a copy of the License at

 * http://wwww.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language govering permissions and
 * limitations under the License.
 */

package org.gcszhn.autocard.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import org.gcszhn.autocard.R;
import org.gcszhn.autocard.component.AnimImage;
import org.gcszhn.autocard.component.CircleImageView;
import org.gcszhn.autocard.config.CancelButtonConfig;
import org.gcszhn.autocard.config.UserConfig;
import org.gcszhn.autocard.service.AutoCardService;

/**
 * 打卡的运行页面
 * @author Zhang.H.N
 * @version 1.0
 * */
public class RunActivity extends BasicActivity {
    /**头像*/
    private CircleImageView imageView;
    /**取消按钮*/
    private Button cancelButon ;
    /**动画*/
    private AnimImage animImage ;
    /**打卡信息描述*/
    private static TextView report;
    /***消息处理，更新信息*/
    private static Handler handler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            report.setText(msg.getData().get("info").toString());
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.run_activity);
        cancelButon = findViewById(R.id.cancelButton);
        imageView = findViewById(R.id.mainView);
        report = findViewById(R.id.report);
        imageView.setImageBitmap(UserConfig.getHeader());
        animImage = new AnimImage(imageView, this, R.anim.icon_animation);
        if (AutoCardService.isRunning()) animImage.start();
        CancelButtonConfig.config(cancelButon, this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence("report", report.getText());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        report.setText(savedInstanceState.getCharSequence("report"));

    }

    /**其他发送更新信息*/
    public static void sendMessage(String msg) {
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("info", msg);
        message.setData(bundle);
        handler.sendMessage(message);
    }
}